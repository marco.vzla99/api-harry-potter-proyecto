import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter_api/dominio/json_personajes.dart';
import 'package:harry_potter_api/dominio/personajes.dart';
import 'dart:io';
void main() {
  test('El Harry esta en el JSon del disco', ()  {
    String json = File('json/personajes.json').readAsStringSync();
    final response = obtenerTodosPersonajes(json);
    final elHarry = Personaje(name: "Harry Potter", alternateNames:[], species: "human", gender: "male", house: "Gryffindor", dateOfBirth: "31-07-1980", yearOfBirth: 1980, wizard: true, ancestry: "half-blood", eyeColour: "green", hairColour: "black", patronus: "stag", hogwartsStudent: true, hogwartsStaff: false, actor: "Daniel Radcliffe",alive: true, image: "https://hp-api.herokuapp.com/images/harry.jpg");
    response.match((l){
      expect(true, equals (false));
    }, (r){
      expect(r[0].name.contains(elHarry.name), equals(true));
    });
  });

  test('Freddy Turbina no esta en el JSon del disco', ()  {
    String json = File('json/personajes.json').readAsStringSync();
    final response = obtenerTodosPersonajes(json);
    final freddy = Personaje(name: "Freddy Turbina", alternateNames:[], species: "human", gender: "male", house: "Slytherin", dateOfBirth: "06-09-1969", yearOfBirth: 1969, wizard: true, ancestry: "half-blood", eyeColour: "brown", hairColour: "black", patronus: "", hogwartsStudent: true, hogwartsStaff: false, actor: "Pedro Jose Montemayor de la Colina de Atizapan de Zaragoza",alive: true, image: "");
    response.match((l){
      expect(false, equals (true));
    }, (r){
      expect(r[0].name.contains(freddy.name), equals(false));
    });
  });  
}