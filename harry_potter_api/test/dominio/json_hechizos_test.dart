import 'dart:io';
import 'package:harry_potter_api/dominio/hechizos.dart';
import 'package:harry_potter_api/dominio/json_hechizos.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Aguamenti esta en el JSon del disco', ()  {
    String json = File('json/hechizos.json').readAsStringSync();
    final response = obtenerTodosHechizos(json);
    final aguamenti = Hechizos(name: "Aguamenti", description: "Summons water");
    response.match((l){
      expect(true, equals (false));
    }, (r){
      expect(r[2].name.contains(aguamenti.name), equals(true));
    });
  });

  test('Kaboom no esta en el JSon del disco', ()  {
    String json = File('json/hechizos.json').readAsStringSync();
    final response = obtenerTodosHechizos(json);
    final kaboom = Hechizos(name: "Kaboom", description: "Explota");
    response.match((l){
      expect(false, equals (true));
    }, (r){
      expect(r[0].name.contains(kaboom.name), equals(false));
    });
  });
  
}