import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter_api/dominio/repositorios.dart';

void main() {
  test('Harry esta en el JSon del disco', ()  {
    RepositorioEnDisco repositorio = RepositorioEnDisco();
    final response = repositorio.obtenerTodosLosJson('general');
    String elHarry = '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    response.match((l) {
      expect(true, equals(false));
    },(r) {
      expect(r.contains(elHarry), equals(true));
    });
  }); 

    test('Harry esta en el JSon real', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('general');
    String elHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(elHarry), equals(true));
    });
  });

  test('Harry esta en el JSon real del Json de estudiantes', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('estudiantes');
    String elHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(elHarry), equals(true));
    });
  });

  test('Draco esta en el JSon real del Json de Slytherin', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('slytherin');
    String elDraco= '{"name":"Draco Malfoy","alternate_names":[],"species":"human","gender":"male","house":"Slytherin","dateOfBirth":"05-06-1980","yearOfBirth":1980,"wizard":true,"ancestry":"pure-blood","eyeColour":"grey","hairColour":"blonde","wand":{"wood":"hawthorn","core":"unicorn tail-hair","length":10},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Tom Felton","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/draco.jpg"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(elDraco), equals(true));
    });
  });

  test('Cho (jajaja) esta en el JSon real del Json de Ravenclaw', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('ravenclaw');
    String elCho= '{"name":"Cho Chang","alternate_names":[],"species":"human","gender":"female","house":"Ravenclaw","dateOfBirth":"","yearOfBirth":null,"wizard":true,"ancestry":"","eyeColour":"brown","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"swan","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Katie Leung","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/cho.jpg"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(elCho), equals(true));
    });
  });

  test('Cedric esta en el JSon real del Json de Hufflepuff', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('hufflepuff');
    String elCedric= '{"name":"Cedric Diggory","alternate_names":[],"species":"human","gender":"male","house":"Hufflepuff","dateOfBirth":"","yearOfBirth":1977,"wizard":true,"ancestry":"","eyeColour":"grey","hairColour":"brown","wand":{"wood":"ash","core":"unicorn hair","length":12.25},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Robert Pattinson","alternate_actors":[],"alive":false,"image":"https://hp-api.herokuapp.com/images/cedric.png"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(elCedric), equals(true));
    });
  });

  test('Minerva esta en el JSon real del Json de staff', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('staff');
    String Minerva= '{"name":"Minerva McGonagall","alternate_names":[],"species":"human","gender":"female","house":"Gryffindor","dateOfBirth":"04-10-1925","yearOfBirth":1925,"wizard":true,"ancestry":"","eyeColour":"","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"tabby cat","hogwartsStudent":false,"hogwartsStaff":true,"actor":"Dame Maggie Smith","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/mcgonagall.jpg"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(Minerva), equals(true));
    });
  });

  test('Aberto esta en el JSon real del Json de hechizos', ()async{
    RepositorioReal repositorio = RepositorioReal();
    final response = await repositorio.obtenerTodosLosJson('hechizos');
    String aber= '{"name":"Aberto","description":"Opens locked doors"}';
    response.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(aber), equals(true));
    });
  });
}