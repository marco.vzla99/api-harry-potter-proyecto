import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

import 'package:harry_potter_api/dominio/hechizos.dart';
import 'package:harry_potter_api/dominio/json_hechizos.dart';
import 'package:harry_potter_api/dominio/json_personajes.dart';
import 'package:harry_potter_api/dominio/personajes.dart';
import 'package:harry_potter_api/dominio/problemas.dart';
import 'package:harry_potter_api/dominio/repositorios.dart';

class EventoVerificacion {}
class Creado extends EventoVerificacion {}
class OpcionRecibida extends EventoVerificacion {
  final String opcion;
  OpcionRecibida (this.opcion);
}

class EstadoVerificacion {}
class Creandose extends EstadoVerificacion {}
class MostrandoPantallaPrincipal extends EstadoVerificacion {}
class SolicitandoCategoria extends EstadoVerificacion {}
class SolicitandoInformacion extends EstadoVerificacion {}
class SinConexion extends EstadoVerificacion {}
class HayError extends EstadoVerificacion {}

class MostrandoListaGeneral extends EstadoVerificacion {
  final List<Personaje> personajes;
  MostrandoListaGeneral(this.personajes);
}

class MostrandoListaEstudiantes extends EstadoVerificacion {
    final List<Personaje> personajes;
  MostrandoListaEstudiantes(this.personajes);
}
class MostrandoListaStaff extends EstadoVerificacion {
    final List<Personaje> personajes;
  MostrandoListaStaff(this.personajes);
}

class MostrandoListaGryffindor extends EstadoVerificacion{
  final List<Personaje> personajes;
  MostrandoListaGryffindor(this.personajes);
}

class MostrandoListaSlytherin extends EstadoVerificacion{
  final List<Personaje> personajes;
  MostrandoListaSlytherin(this.personajes);
}

class MostrandoListaRavenclaw extends EstadoVerificacion{
  final List<Personaje> personajes;
  MostrandoListaRavenclaw(this.personajes);
}

class MostrandoListaHufflepuff extends EstadoVerificacion{
  final List<Personaje> personajes;
  MostrandoListaHufflepuff(this.personajes);
}
class MostrandoListaHechizos extends EstadoVerificacion {
    final List<Hechizos> hechizos;
    MostrandoListaHechizos(this.hechizos);
}



class BlocVerificacion extends Bloc<EventoVerificacion,EstadoVerificacion>{

  RepositorioReal repositorio = RepositorioReal();
  BlocVerificacion() : super (Creandose()){
    on <Creado>((event, emit) {
      emit(MostrandoPantallaPrincipal());
    });
    on<OpcionRecibida>((event, emit) async {
      emit(SolicitandoInformacion());
      if(event.opcion == 'general'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaGeneral(r));
          });          
        });
      }

      if(event.opcion == 'staff'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaStaff(r));
          });          
        });
      }

      if(event.opcion == 'estudiantes'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaEstudiantes(r));
          });          
        });
      }

      if(event.opcion == 'gryffindor'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaGryffindor(r));
          });          
        });
      }

      if(event.opcion == 'slytherin'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaSlytherin(r));
          });          
        });
      }

      if(event.opcion == 'ravenclaw'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaRavenclaw(r));
          });          
        });
      }

      if(event.opcion == 'hufflepuff'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosPersonajes(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaHufflepuff(r));
          });          
        });
      }

      if(event.opcion == 'hechizos'){
        final response = await repositorio.obtenerTodosLosJson(event.opcion);
        response.match((l) {
          if (l is ServidorNoAlcanzado){
            emit (SinConexion());
          }
        },(r) {
          final responseFinal = obtenerTodosHechizos(r);
          responseFinal.match((l){
            emit(HayError());
          },(r){
            emit(MostrandoListaHechizos(r));
          });          
        });
      }
    });
   }
}