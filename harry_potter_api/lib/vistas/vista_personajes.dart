import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:harry_potter_api/caracteristicas/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter_api/dominio/personajes.dart';

class VistaPersonajes extends StatelessWidget {
  final List<Personaje> personajes;
  const VistaPersonajes(this.personajes,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
     return Column(
      children: [
        TextButton(onPressed: () {
          elBloc.add(Creado());
        },child: const Text('Volver')),
      ListView.builder(
      itemCount: personajes.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(personajes[index].name),
          leading: CachedNetworkImage(imageUrl: '${personajes[index].image}',),
          onTap: (){
          content:SingleChildScrollView(
            child: Column(
              children: [
                Text('Alias: ${personajes[index].alternateNames}'),
                Text('Especie: ${personajes[index].species}'),
                Text('Genero: ${personajes[index].gender}'),
                Text('Casa: ${personajes[index].house}'),
                Text('Fecha de nacimiento: ${personajes[index].dateOfBirth}'),
                Text('Es Mago: ${personajes[index].wizard}'),
                Text('Ascendencia: ${personajes[index].ancestry}'),
                Text('Color de Ojo: ${personajes[index].eyeColour}'),
                Text('Color de cabello: ${personajes[index].hairColour}'),
                Text('Patronus: ${personajes[index].patronus}'),
                Text('Es estudiante: ${personajes[index].hogwartsStudent}'),
                Text('Es staff: ${personajes[index].hogwartsStaff}'),
                Text('Actor: ${personajes[index].actor}'),
                Text('Esta vivo: ${personajes[index].alive}'),
            ],
          ),
        ); 
        },
      );
      },
      ) 
      ],
     );
  }
}