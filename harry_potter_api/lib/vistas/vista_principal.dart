import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter_api/caracteristicas/bloc.dart';

class VistaPantallaPrincipal extends StatelessWidget {
  const VistaPantallaPrincipal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();

    return Column(
      children: [
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('general'));
        }, child: const Text('Ver todos los personajes')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('staff'));
        }, child: const Text('Ver Staff')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('estudiantes'));
        }, child: const Text('Ver estudiantes')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('gryffindor'));
        }, child: const Text('Ver miembros de Gryffindor')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('slytherin'));
        }, child: const Text('Ver miembros de Slytherin')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('ravenclaw'));
        }, child: const Text('Ver miembros de Ravenclaw')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('hufflepuff'));
        }, child: const Text('Ver miembros de Hufflepuff')),
        TextButton(onPressed: (){
          elBloc.add(OpcionRecibida('hechizos'));
        }, child: const Text('Ver hechizos')),
      ],
    );
  }
}