import 'package:flutter/material.dart';
import 'package:harry_potter_api/caracteristicas/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VistaError extends StatelessWidget {
  const VistaError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          // ignore: prefer_const_constructors
          Text('Tiembla. Ocurrio un error',
          ),
          TextButton(onPressed: (){
            elBloc.add(Creado());
          }, child: const Text('Volver'))
        ],
      ),
    );
  }
}