import 'package:flutter/material.dart';
import 'package:harry_potter_api/caracteristicas/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VistaSinRed extends StatelessWidget {
  const VistaSinRed({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          // ignore: prefer_const_constructors
          Text(
            'No hay conexion a Internet. Ve a que te de el aire, abraza a tu familia.',
          ),
          TextButton(onPressed: (){
            elBloc.add(Creado());
          }, child: const Text('Volver'))
        ],
      ),
    );
  }
}