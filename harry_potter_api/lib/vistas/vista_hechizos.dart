import 'package:flutter/material.dart';
import 'package:harry_potter_api/caracteristicas/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter_api/dominio/hechizos.dart';

class VistaHechizos extends StatelessWidget {
  final List<Hechizos> hechizos;
  const VistaHechizos(this.hechizos,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Column(
      children: [ 
        TextButton(onPressed: () {
          elBloc.add(Creado());
        },child: const Text('Volver')),
      ListView.builder(
      itemCount: hechizos.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(hechizos[index].name, textAlign: TextAlign.center ),
          subtitle: Text('Descripcion ${hechizos[index].description}'),
      );
      },
    )
    ],
    );
  }
}