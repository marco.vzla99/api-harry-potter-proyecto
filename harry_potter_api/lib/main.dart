import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter_api/vistas/vista_creandose.dart';
import 'package:harry_potter_api/vistas/vista_error.dart';
import 'package:harry_potter_api/vistas/vista_hechizos.dart';
import 'package:harry_potter_api/vistas/vista_personajes.dart';
import 'package:harry_potter_api/vistas/vista_principal.dart';
import 'package:harry_potter_api/vistas/vista_sin_red.dart';

import 'caracteristicas/bloc.dart';

void main() {  
  runApp(const AplicacionInyectada());
}

class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        BlocVerificacion blocVerificacion =BlocVerificacion();
        Future.delayed(const Duration(seconds: 2),(){
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}
class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Builder(builder: (context) {
          var estado = context.watch<BlocVerificacion>().state;
          if (estado is Creandose){
            return const Center(child: VistaCreandose());
          }
          if (estado is SinConexion){
            return const Center(child: VistaSinRed());
          }
          if (estado is HayError){
            return const Center(child: VistaError());
          }
          if (estado is SolicitandoInformacion){
            return const Center(child: VistaCreandose());
          }
          if (estado is MostrandoPantallaPrincipal){
            return const Center(child: VistaPantallaPrincipal());
          }
          if (estado is MostrandoListaGeneral){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaEstudiantes){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaStaff){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaGryffindor){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaSlytherin){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaRavenclaw){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaHufflepuff){
            return  Center(child: VistaPersonajes(estado.personajes));
          }
          if (estado is MostrandoListaHechizos){
            return  Center(child: VistaHechizos(estado.hechizos));
          }
          return const Center(
            child: Text('Si estas viendo esto, algo salio mal, HUYE'));
        },)
        ),
    );
  }
}