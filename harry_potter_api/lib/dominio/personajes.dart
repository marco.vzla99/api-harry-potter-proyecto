//     final personajes = personajesFromJson(jsonString);
import 'dart:convert';
const String mensajeCampoVacio = 'Sin valor';

class Personaje {
    Personaje({
        required this.name,
        required this.alternateNames,
        required this.species,
        required this.gender,
        required this.house,
        required this.dateOfBirth,
        required this.yearOfBirth,
        required this.wizard,
        required this.ancestry,
        required this.eyeColour,
        required this.hairColour,
        //this.wand,
        required this.patronus,
        required this.hogwartsStudent,
        required this.hogwartsStaff,
        required this.actor,
        required this.alive,
        required this.image,
    });

    final String name;
    final List<dynamic> alternateNames;
    final String species;
    final String gender;
    final String house;
    final String dateOfBirth;
    final int yearOfBirth;
    final bool wizard;
    final String ancestry;
    final String eyeColour;
    final String hairColour;
    //final Wand ? wand;
    final String  patronus;
    final bool hogwartsStudent;
    final bool hogwartsStaff;
    final String  actor;
    final bool  alive;
    final String image;

    factory Personaje.fromJson(Map<String, dynamic> json) => Personaje(
        name: json["name"].toString().isEmpty ? mensajeCampoVacio: json['name'],
        alternateNames: json["alternate_names"] ?? [],
        species: json["species"].toString().isEmpty ? mensajeCampoVacio: json['species'],
        gender: json["gender"].toString().isEmpty ? mensajeCampoVacio: json['gender'],
        house: json["house"].toString().isEmpty ? mensajeCampoVacio: json['house'],
        dateOfBirth: json["dateOfBirth"].toString().isEmpty ? mensajeCampoVacio: json['dateOfBirth'],
        yearOfBirth: json["yearOfBirth"] ?? 0,
        wizard: json["wizard"].toString().isEmpty ? mensajeCampoVacio: json['wizard'],
        ancestry: json["ancestry"].toString().isEmpty ? mensajeCampoVacio: json['ancestry'],
        eyeColour: json["eyeColour"].toString().isEmpty ? mensajeCampoVacio: json['eyeColour'],
        hairColour: json["hairColour"].toString().isEmpty ? mensajeCampoVacio: json['hairColour'],
        //wand: Wand.fromJson(json["wand"]),
        patronus: json["patronus"].toString().isEmpty ? mensajeCampoVacio: json['patronus'],
        hogwartsStudent: json["hogwartsStudent"].toString().isEmpty ? mensajeCampoVacio: json['hogwartsStudent'],
        hogwartsStaff: json["hogwartsStaff"].toString().isEmpty ? mensajeCampoVacio: json['hogwartsStaff'],
        actor: json["actor"].toString().isEmpty ? mensajeCampoVacio: json['actor'],        
        alive: json["alive"].toString().isEmpty ? mensajeCampoVacio: json['alive'],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "alternate_names": alternateNames,
        "species": species,
        "gender": gender,
        "house": house,
        "dateOfBirth": dateOfBirth,
        "yearOfBirth": yearOfBirth,
        "wizard": wizard,
        "ancestry": ancestry,
        "eyeColour": eyeColour,
        "hairColour": hairColour,
        //"wand": wand?.toJson(),
        "patronus": patronus,
        "hogwartsStudent": hogwartsStudent,
        "hogwartsStaff": hogwartsStaff,
        "actor": actor,        
        "alive": alive,
        "image": image,
    };
}

/*class Wand {
    Wand({
        this.wood,
        this.core,
        this.length,
    });

    String ? wood;
    String ? core;
    int ? length;

    factory Wand.fromJson(Map<String, dynamic> json) => Wand(
        wood: json["wood"],
        core: json["core"],
        length: json["length"] == null ? null : json["length"],
    );

    Map<String, dynamic> toJson() => {
        "wood": wood,
        "core": core,
        "length": length == null ? null : length,
    };
}*/