import 'dart:convert';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter_api/dominio/personajes.dart';
import 'package:harry_potter_api/dominio/problemas.dart';

// https://hp-api.onrender.com/api/characters/staff (Ya)
// https://hp-api.onrender.com/api/characters (Ya)
// https://hp-api.onrender.com/api/characters/house/gryffindor
// https://hp-api.onrender.com/api/characters/students (Ya)
// https://hp-api.onrender.com/api/spells (Ya)

  Either<Problema,List<Personaje>> obtenerTodosPersonajes(String json) {
    try {
      List<dynamic> personajes = jsonDecode(json);
      return Right(personajes.map((e) => Personaje.fromJson(e)).toList());
    } catch (e) {
      return Left(JsonIncorrecto());
    }     
  }
