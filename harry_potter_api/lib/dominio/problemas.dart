abstract class Problema {}

class VersionIncorrectaJson extends Problema {}
class JsonIncorrecto extends Problema {}
class ServidorNoAlcanzado extends Problema {}