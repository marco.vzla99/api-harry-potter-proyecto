import 'dart:io';
import 'package:harry_potter_api/dominio/problemas.dart';
import 'package:http/http.dart' as http;
import 'package:fpdart/fpdart.dart';

// https://hp-api.onrender.com/api/characters/staff (Ya)
// https://hp-api.onrender.com/api/characters (Ya)
// https://hp-api.onrender.com/api/characters/house/gryffindor (Ya)
// https://hp-api.onrender.com/api/characters/students (Ya)
// https://hp-api.onrender.com/api/spells (Ya)
class RepositorioReal{

  Future<Either<Problema, String>> obtenerTodosLosJson(String opcion) async {
    String json='';
    if (opcion == 'general'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'gryffindor'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/gryffindor');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'slytherin'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/slytherin');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'ravenclaw'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/ravenclaw');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'hufflepuff'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/hufflepuff');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'staff'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/staff');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'estudiantes'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/students');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }

    if (opcion == 'hechizos'){
      Uri direccion = Uri.http('hp-api.onrender.com', 'api/spells');
      final response = await http.get(direccion);
      if (response.statusCode != 200){
        return Left(ServidorNoAlcanzado());
      }
      json = response.body;      
    }
    
    return Right(json);
  }
}
class RepositorioEnDisco{
  Either<Problema, String> obtenerTodosLosJson(String opcion)  {
    String json='';

    if (opcion == 'staff'){
      try {
        json = File('json/staff.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      }      
    }

    if (opcion == 'gryffindor'){
      try {
        json = File('json/gryffindor.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      } 
    }

    if (opcion == 'slytherin'){
      try {
        json = File('json/slytherin.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      } 
    }

    if (opcion == 'ravenclaw'){
      try {
        json = File('json/ravenclaw.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      } 
    }

    if (opcion == 'hufflepuff'){
      try {
        json = File('json/hufflepuff.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      } 
    }
    if (opcion == 'estudiantes'){
      try {
        json = File('json/estudiantes.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      }     
    }

    if (opcion == 'hechizos'){
      try {
        json = File('json/hechizos.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      }      
    }

    if (opcion == 'general'){
      try {
        json = File('json/personajes.json').readAsStringSync();
      } catch (e) {
        return Left(JsonIncorrecto());
      } 
    }
    
    return Right(json);
  }
}