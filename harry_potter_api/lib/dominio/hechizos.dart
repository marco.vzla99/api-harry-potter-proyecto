//     final hechizos = hechizosFromJson(jsonString);
import 'dart:convert';

//List<Hechizos> hechizosFromJson(String str) => List<Hechizos>.from(json.decode(str).map((x) => Hechizos.fromJson(x)));
//String hechizosToJson(List<Hechizos> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Hechizos {
    Hechizos({
        required this.name,
        required this.description,
    });

    String name;
    String description;

    factory Hechizos.fromJson(Map<String, dynamic> json) => Hechizos(
        name: json["name"],
        description: json["description"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "description": description,
    };
} 