import 'dart:convert';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter_api/dominio/hechizos.dart';
import 'package:harry_potter_api/dominio/problemas.dart';
  
  Either<Problema,List<Hechizos>> obtenerTodosHechizos(String json) {
    try {
      List<dynamic> hechizos = jsonDecode(json);
      return Right(hechizos.map((e) => Hechizos.fromJson(e)).toList());
    } catch (e) {
      return Left(JsonIncorrecto());
    }     
  }

